package com.weatherunlocked;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * Cucumber Step Definitions for validating the Current Weather.
 *
 * @author Nick G.
 */
public class CurrentWeatherStepDefs {

    private static final String BASE_URI = "http://api.weatherunlocked.com";
    private static final String APP_ID = "";
    private static final String APP_KEY = "";

    private String latLong = null;
    private Response response = null;

    @Given("I know the latitude and longitude")
    public void i_know_the_latitude_and_longitude() {
        latLong = "51.50,-0.12";
    }

    @When("I request the weather")
    public void i_request_the_weather() {

        response = given().
                baseUri(BASE_URI).
                queryParam("app_id", APP_ID).
                queryParam("app_key", APP_KEY).
                get("/api/current/51.50,-0.12").then().extract().response();

    }

    @Then("the server returns the current weather")
    public void the_server_returns_the_current_weather() {
        response.prettyPrint(); // simply print out the response

        // get the Weather Desc (wx_desc)
        JsonPath jsonPath = response.jsonPath();
        String weatherDesc = jsonPath.get("wx_desc");
        System.out.println("Weather Description: " + weatherDesc);
    }
}
