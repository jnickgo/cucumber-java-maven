package com.weatherunlocked;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Cucumber task runner Class.
 *
 * @author Nick G.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources")
public class RunCucumberTest {



}
