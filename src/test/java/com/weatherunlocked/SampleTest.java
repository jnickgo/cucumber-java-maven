package com.weatherunlocked;

import org.junit.Ignore;
import org.junit.Test;

import static io.restassured.RestAssured.*;


/**
 * Simple JUnit test to debug service.
 * Note. Ignoring as the focus is on Cucumber tests.
 *
 * @author Nick G
 */
public class SampleTest {

    private static final String APP_ID = "";
    private static final String APP_KEY = "";

    @Ignore
    @Test
    public void getCurrentWeatherByLatLong() {
        baseURI = "http://api.weatherunlocked.com/api/current";

        given().
                baseUri(baseURI).
                queryParam("app_id", APP_ID).
                queryParam("app_key", APP_KEY).
                get("/51.50,-0.12").then().log().all();
    }

    @Ignore
    @Test
    public void getCurrentWeatherByZipCode() {
        baseURI = "http://api.weatherunlocked.com/api/current";

        given().
                baseUri(baseURI).
                queryParam("app_id", APP_ID).
                queryParam("app_key", APP_KEY).
                get("us.64155").then().log().all();

    }
}
