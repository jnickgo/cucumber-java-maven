Feature: I get the current weather

  Scenario: I search by latitude and longitude
    Given I know the latitude and longitude
    When I request the weather
    Then the server returns the current weather

  # Given is not required
  Scenario: I search by country and zip
    When I request the weather
    Then the server returns the current weather